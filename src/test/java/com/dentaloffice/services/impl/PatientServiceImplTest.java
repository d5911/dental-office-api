package com.dentaloffice.services.impl;

//import com.dentaloffice.exceptions.PatientNotFoundException;
import com.dentaloffice.models.Patient;
import com.dentaloffice.repositories.PatientRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PatientServiceImplTest {
    @Mock
    private PatientRepository patientRepository;

    private PatientServiceImpl patientService;

    @BeforeEach
    public void setup() {
        patientService = new PatientServiceImpl(patientRepository);
    }

    @Test
    void shouldSavePatient() {
//        given data
        Patient patientFromUserInput = new Patient(
                null, "Dragana",
                "Spasojevic",
                "04.11.1989.",
                "017673831708",
                null);
//data after saving patient
        UUID id = UUID.fromString("eefcbdd4-3cc3-4b93-822c-6226305677cd");
        Patient createdPatient = new Patient(
                id, "Dragana",
                "Spasojevic",
                "04.11.1989.",
                "017673831708",
                null);

//        repository is called with data from user input and repository returned created patient with id
        when(patientRepository.save(any(Patient.class))).thenReturn(createdPatient);

//         service is called with data from user input
        Patient result = patientService.save(patientFromUserInput);

//        check is the repository called with value from user input
        verify(patientRepository).save(patientFromUserInput);

//        compare returned result is equal to created patient
        assertThat(result).isEqualTo(createdPatient);
    }

    @Test
    void schouldFindAll() {

        List<Patient> patients = List.of(
                new Patient(UUID.randomUUID(), "Dragana", "Spasojevic", "04.11.1989.", "017673831708", null)
        );

        when(patientRepository.findByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(
                eq("Dragana"), eq("Dragana"), any(Pageable.class)
        )).thenReturn(new PageImpl<>(patients));

        patientService.findAll("Dragana", 1, 10, "Dragana", true);

        verify(patientRepository).findByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(
                eq("Dragana"), eq("Dragana"), any(Pageable.class)
        );

    }

    @Test
    void shouldDelete() {

        UUID id = UUID.fromString("eefcbdd4-3cc3-4b93-822c-6226305677cd");

        patientService.delete(id);
        verify(patientRepository).deleteById(id);
    }

    @Nested
    class Exists {
        @Test
        void shouldExists() {

            UUID id = UUID.fromString("eefcbdd4-3cc3-4b93-822c-6226305677cd");
            Patient createdPatient = new Patient(
                    id, "Dragana",
                    "Spasojevic",
                    "04.11.1989.",
                    "017673831708",
                    null);

            when(patientRepository.existsById(id)).thenReturn(true);

            boolean result = patientService.exists(createdPatient.getId());

            verify(patientRepository).existsById(createdPatient.getId());

            assertThat(result).isTrue();
        }

        @Test
        void shouldNotExist() {

            UUID id = UUID.fromString("eefcbdd4-3cc3-4b93-822c-6226305677cd");

            when(patientRepository.existsById(id)).thenReturn(false);

            boolean result = patientService.exists(id);

            assertThat(result).isFalse();
        }
    }

    @Test
    void shouldEdit() {

        UUID id = UUID.fromString("eefcbdd4-3cc3-4b93-822c-6226305677cd");
        Patient patientToBeEdited = new Patient(
                id, "Dragana",
                "Spasojevic",
                "04.11.1989.",
                "017673831708",
                null);

        Patient editedPatient = new Patient();
        editedPatient.setId(patientToBeEdited.getId());
        editedPatient.setFirstName("Dragana");
        editedPatient.setLastName("Blazanovic");
        editedPatient.setBirthDate("04.11.1989.");
        editedPatient.setPhoneNumber("17673831708");
        editedPatient.setRecords(null);

        when(patientRepository.save(any(Patient.class))).thenReturn(editedPatient);

        Patient result = patientService.edit(patientToBeEdited);

        verify(patientRepository).save(patientToBeEdited);

        assertThat(result).isEqualTo(editedPatient);
    }

    @Test
    void shouldThrowExceptionWhenPatientIdIsNull() {
        Patient patientToBeEdited = new Patient();
        patientToBeEdited.setId(null);

        assertThrows(IllegalArgumentException.class, () -> patientService.edit(patientToBeEdited));

        verify(patientRepository, times(0)).save(any(Patient.class));
    }


    @Nested
    class GetPatient {
        @Test
        void shouldGet() {

            UUID id = UUID.fromString("d8070033-c1d8-4f5f-9032-e43f7d6e10c1");
            Patient createdPatient = new Patient(
                    id,
                    "Dragana",
                    "Spasojevic",
                    "04.11.1989.",
                    "017673831708",
                    null);

            when(patientRepository.findById(id)).thenReturn(Optional.of(createdPatient));

            Patient result = patientService.get(id);

            verify(patientRepository).findById(id);

            assertThat(result.getId()).isEqualTo(createdPatient.getId());
        }

        @Test
        void shouldThrowExceptionWhenPatientNotFound() {
            UUID id = UUID.randomUUID();

            when(patientRepository.findById(id)).thenReturn(Optional.empty());

            assertThatThrownBy(() -> patientService.get(id))
                    .isInstanceOf(NoSuchElementException.class)
                    .hasMessageContaining("No value present");

            verify(patientRepository).findById(id);
        }
    }
}
package com.dentaloffice.services.impl;

import com.dentaloffice.models.Material;
import com.dentaloffice.models.Record;
import com.dentaloffice.repositories.MaterialRepository;
import com.dentaloffice.repositories.RecordRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MaterialServiceImplTest {
    @Mock
    private MaterialRepository materialRepository;

    @Mock
    private RecordRepository recordRepository;

    private MaterialServiceImpl materialService;

    private RecordServiceImpl recordService;

    @BeforeEach
    public void setup() {
        materialService = new MaterialServiceImpl(materialRepository, recordRepository);
    }

    @Test
    void ShouldSaveMaterial() {
        Material materialFromUserInput = new Material(
                null,
                null,
                "Pads",
                982);

        UUID id = UUID.randomUUID();

        Material createdMaterial = new Material(
                id,
                null,
                "Pads",
                982);

        when(materialRepository.save(any(Material.class))).thenReturn(createdMaterial);

        Material result = materialService.save(materialFromUserInput);

        verify(materialRepository).save(materialFromUserInput);

        assertThat(result).isEqualTo(createdMaterial);
    }

    @Test
    void shouldFindAll() {

        List<Material> materials = List.of(
                new Material(UUID.randomUUID(), null, "Pads", 5)
        );

        Page<Material> pageOfMaterials = new PageImpl<>(materials);

        when(materialRepository.findByMaterialNameContainingIgnoreCase(eq("Pads"), any(Pageable.class)
        )).thenReturn(pageOfMaterials);

        Page<Material> result = materialService.findAll("Pads", 1, 10, "Pads", true);

        verify(materialRepository).findByMaterialNameContainingIgnoreCase(eq("Pads"), any(Pageable.class));

        assertThat(result).isEqualTo(pageOfMaterials);

    }

    @Nested
    class Exists {
        @Test
        void shouldExist() {
            UUID id = UUID.randomUUID();

            Material createdMaterial = new Material(
                    id,
                    null,
                    "Pads",
                    982);

            when(materialRepository.existsById(id)).thenReturn(true);

            boolean result = materialService.exists(id);

            verify(materialRepository).existsById(createdMaterial.getId());

            assertThat(result).isTrue();
        }

        @Test
        void shouldNotExist() {
            UUID id = UUID.randomUUID();

            when(materialRepository.existsById(id)).thenReturn(false);

            boolean result = materialService.exists(id);

            assertThat(result).isFalse();
        }
    }

    @Test
    void shouldDelete() {

        UUID id = UUID.randomUUID();

        materialService.delete(id);

        verify(materialRepository).deleteById(id);
    }

    @Test
    void shouldEdit() {

        UUID id = UUID.randomUUID();

        Material materialToBeEdited = new Material(
                id,
                null,
                "Pads",
                5
        );

        Material editedMaterial = new Material();
        editedMaterial.setMaterialName("Scissors");
        editedMaterial.setId(id);
        editedMaterial.setEnrolledMaterialsInRecord(null);
        editedMaterial.setQuantity(10);

        when(materialRepository.save(any(Material.class))).thenReturn(editedMaterial);

        Material result = materialService.edit(materialToBeEdited);

        verify(materialRepository).save(materialToBeEdited);

        assertThat(result).isEqualTo(editedMaterial);
    }

    @Test
    void shouldThrowExceptionWhenMaterialIsNull() {
        Material materialToBeEdited = new Material();
        materialToBeEdited.setId(null);

        assertThrows(IllegalArgumentException.class, () -> materialService.edit(materialToBeEdited));

        verify(materialRepository, times(0)).save(any(Material.class));
    }

    @Nested
    class GetMaterial {
        @Test
        void shouldGet() {
            UUID id = UUID.randomUUID();

            Material createdMaterial = new Material(
                    id,
                    null,
                    "Pads",
                    982);

            when(materialRepository.findById(id)).thenReturn(Optional.of(createdMaterial));

            Material result = materialService.get(id);

            verify(materialRepository).findById(id);

            assertThat(result.getId()).isEqualTo(createdMaterial.getId());
        }

        @Test
        void shouldThrowExceptionWhenMaterialNotFound() {
            UUID id = UUID.randomUUID();

            when(materialRepository.findById(id)).thenReturn(Optional.empty());

            assertThatThrownBy(() -> materialService.get(id))
                    .isInstanceOf(NoSuchElementException.class)
                    .hasMessageContaining("No value present");

            verify(materialRepository).findById(id);
        }
    }

    @Test
    void shouldGetEnrolledMaterialsInRecord() {

        UUID id = UUID.fromString("f1a249cf-22db-4dae-b71a-8fdcb915eb3b");

        Material material1 = new Material();
        material1.setId(UUID.fromString("44605bfb-c4d7-4c07-93fa-e631a9e6eff6"));
        material1.setEnrolledMaterialsInRecord(null);
        material1.setMaterialName("Pads");
        material1.setQuantity(982);

        Material material2 = new Material();
        material2.setId(UUID.fromString("fd8cf642-9377-4ea6-8033-440cef735f6a"));
        material2.setEnrolledMaterialsInRecord(null);
        material2.setMaterialName("Injections");
        material2.setQuantity(148);

        Material material3 = new Material();
        material3.setId(UUID.fromString("fbd71975-6199-4b67-aa96-aed76e61a03e"));
        material3.setEnrolledMaterialsInRecord(null);
        material3.setMaterialName("Alcohol");
        material3.setQuantity(498);

        List<Material> materials = Arrays.asList(material1, material2, material3);

        when(materialRepository.findByEnrolledMaterialsInRecordId(id)).thenReturn(materials);

        List<Material> result = materialService.getMaterials(id);

        verify(materialRepository, times(1)).findByEnrolledMaterialsInRecordId(id);
        assertNotNull(result);
    }

    @Test
    void shouldAddUsedMaterialsToTheRecord() throws Exception {

        UUID materialId = UUID.randomUUID();
        UUID recordId = UUID.randomUUID();

        Material material = new Material();
        material.setEnrolledMaterialsInRecord(new ArrayList<>());

        Record record = new Record();

        when(materialRepository.getById(materialId)).thenReturn(material);
        when(recordRepository.getById(recordId)).thenReturn(record);
        when(recordRepository.findByIdAndMaterialsId(recordId,materialId)).thenReturn(Optional.empty());

        materialService.addRecord(materialId, recordId);

        assertTrue(material.getEnrolledMaterialsInRecord().contains(record));
        verify(materialRepository).getById(materialId);
        verify(recordRepository).getById(recordId);
        verify(recordRepository).findByIdAndMaterialsId(recordId, materialId);
    }
}
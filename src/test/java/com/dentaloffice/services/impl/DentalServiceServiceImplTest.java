package com.dentaloffice.services.impl;

import com.dentaloffice.models.DentalService;
import com.dentaloffice.repositories.DentalServiceRepository;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DentalServiceServiceImplTest {

    @Mock
    private DentalServiceRepository dentalServiceRepository;

    @InjectMocks
    private DentalServiceServiceImpl dentalServiceService;

    @Nested
    class Save {
        @Test
        void shouldThrowIllegalArgumentException_whenEntityIsNull() {
            IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> dentalServiceService.save(null));

            assertEquals("Dental service must not be null", exception.getMessage());
        }

        @Test
        void shouldSave() {
            DentalService dentalServiceFromUserInput = new DentalService(
                    null,
                    null,
                    "Extraction"
            );

            UUID id = UUID.fromString("8d6ba0c2-29e3-436f-9890-285928199e60");

            DentalService createdDentalService = new DentalService(
                    id,
                    null,
                    "Extraction"
            );

            when(dentalServiceRepository.save(any())).thenReturn(createdDentalService);

            DentalService result = dentalServiceService.save(dentalServiceFromUserInput);

            verify(dentalServiceRepository).save(dentalServiceFromUserInput);

            assertThat(result).isEqualTo(createdDentalService);
        }
    }

    @Test
    void shouldFindAll() {

        List<DentalService> dentalServiceList = List.of(new DentalService(UUID.randomUUID(), null, "Extraction"));

        Page<DentalService> dentalServicePage = new PageImpl<>(dentalServiceList);

        when(dentalServiceRepository.findByServiceNameContainingIgnoreCase(eq("Extraction"), any(Pageable.class)))
                .thenReturn(dentalServicePage);

        Page<DentalService> result = dentalServiceService.findAll("Extraction", 1, 10, "Extraction", true);

        verify(dentalServiceRepository).findByServiceNameContainingIgnoreCase(eq("Extraction"), any(Pageable.class));

        assertThat(result).isEqualTo(dentalServicePage);
    }

    @Nested
    class GetDentalService {
        @Test
        void shouldGet() {

            UUID id = UUID.fromString("4390436e-d32e-4125-bd83-4cc5c7e042f2");

            DentalService createdDentalService = new DentalService(
                    id,
                    null,
                    "Extraction"
            );

            when(dentalServiceRepository.findById(id)).thenReturn(Optional.of(createdDentalService));

            DentalService result = dentalServiceService.get(id);

            verify(dentalServiceRepository).findById(id);

            assertThat(result.getId()).isEqualTo(createdDentalService.getId());
        }

        @Test
        void shouldThrowExceptionWhenDentalServiceNotFound() {

            UUID id = UUID.fromString("4200a9ef-6353-42c7-ac49-1c1dfb9981dd");

            when(dentalServiceRepository.findById(id)).thenReturn(Optional.empty());

            assertThatThrownBy(() -> dentalServiceService.get(id))
                    .isInstanceOf(NoSuchElementException.class);

            verify(dentalServiceRepository).findById(id);
        }
    }

    @Nested
    class Exists {

        @Test
        void shouldThrowIllegalArgumentExceptionWhenIdIsNull() {

            IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> dentalServiceService.exists(null));

            assertEquals("Id must not be null", exception.getMessage());
        }

        @Test
        void shouldExist() {
            UUID id = UUID.fromString("9af1215e-3374-43a2-bf30-b7de94526551");

            DentalService createdDentalService = new DentalService(
                    id,
                    null,
                    "Extraction"
            );

            when(dentalServiceRepository.existsById(id)).thenReturn(true);

            boolean result = dentalServiceService.exists(id);

            verify(dentalServiceRepository).existsById(createdDentalService.getId());

            assertThat(result).isTrue();
        }

        @Test
        void shouldNotExist() {
            UUID id = UUID.fromString("95dd1751-76d0-4d9d-83b8-625fb9dc5406");

            when(dentalServiceRepository.existsById(id)).thenReturn(false);

            boolean result = dentalServiceService.exists(id);

            assertThat(result).isFalse();
        }
    }

    @Nested
    class Delete {

        @Test
        void shouldThrowIllegalArgumentExceptionWhenIdIsNull() {
            IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> dentalServiceService.delete(null));

            assertEquals("Id must not be null", exception.getMessage());
        }

        @Test
        void shouldDelete() {
            UUID id = UUID.fromString("8271c006-ce25-4a8b-8220-ef3c5442ae29");

            doNothing().when(dentalServiceRepository).deleteById(id);

            dentalServiceService.delete(id);

            verify(dentalServiceRepository, times(1)).deleteById(id);
        }
    }


    @Test
    void shouldEdit() {
        UUID id = UUID.fromString("b7e68667-3472-4ada-99c7-e520fb5b6df8");

        DentalService dentalServiceToBeEdited = new DentalService(
                id,
                null,
                "Extraction"
        );

        DentalService editedDentalService = new DentalService();
        editedDentalService.setServiceName("Tooth decay");
        editedDentalService.setId(id);
        editedDentalService.setEnrolledServicesInRecord(null);

        when(dentalServiceRepository.save(any())).thenReturn(editedDentalService);

        DentalService result = dentalServiceService.edit(dentalServiceToBeEdited);

        verify(dentalServiceRepository).save(dentalServiceToBeEdited);

        assertThat(result).isEqualTo(editedDentalService);
    }

    @Nested
    class GetService {
        @Test
        void shouldGet() {
            UUID id = UUID.fromString("4befd378-9c1b-48ae-8805-65de2a125c71");

            DentalService createdDentalService = new DentalService(
                    id,
                    null,
                    "Extraction"
            );

            when(dentalServiceRepository.findById(id)).thenReturn(Optional.of(createdDentalService));

            DentalService result = dentalServiceService.get(id);

            verify(dentalServiceRepository).findById(id);

            assertThat(result.getId()).isEqualTo(createdDentalService.getId());
        }

        @Test
        void shouldThrowExceptionWhenDentalServiceNotFound () {
            UUID id = UUID.fromString("e8dc743d-daa7-40a4-9dfe-98b3868c1e8a");

            when(dentalServiceRepository.findById(id)).thenReturn(Optional.empty());

            assertThatThrownBy(() -> dentalServiceService.get(id))
                    .isInstanceOf(NoSuchElementException.class);

            verify(dentalServiceRepository).findById(id);
        }
    }
}
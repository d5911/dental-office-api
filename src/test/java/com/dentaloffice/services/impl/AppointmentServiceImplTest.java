package com.dentaloffice.services.impl;

import com.dentaloffice.models.Appointment;
import com.dentaloffice.models.Patient;
import com.dentaloffice.repositories.AppointmentRepository;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AppointmentServiceImplTest {

    @Mock
    private AppointmentRepository appointmentRepository;

    @InjectMocks
    private AppointmentServiceImpl appointmentService;

    @Nested
    class Save {
        @Test
        void shouldThrowIllegalArgumentException_whenEntityIsNull() {
            IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> appointmentService.save(null));

            assertEquals("Appointment must not be null", exception.getMessage());
        }

        @Test
        void shouldSaveWhenEntityIsValid() {

            Patient patient = mock(Patient.class);
            Appointment inputAppointment = new Appointment(null, patient, new Date(), "12:45");
            Appointment savedAppointment = new Appointment (UUID.fromString("9f0ec821-525e-457e-aae5-17be41faa9e1"), patient, new Date(), "12:45");

            when(appointmentRepository.save(any())).thenReturn(savedAppointment);

            Appointment result = appointmentService.save(inputAppointment);

            verify(appointmentRepository).save(inputAppointment);

            assertThat(result).isEqualTo(savedAppointment);
        }
    }

    @Test
    void shouldFindAll() {

        Patient patient = mock(Patient.class);

        List<Appointment> appointmentList = List.of(
                        new Appointment(UUID.fromString("78996ed7-60a5-4a64-a850-bfb96af04d30"),
                        patient,
                        new Date(),
                        "12:45"));

        Page<Appointment> appointmentPage = new PageImpl<>(appointmentList);

        when(appointmentRepository.findByPatientFirstNameContainingIgnoreCaseOrPatientLastNameContainingIgnoreCase(eq("Dragana"), any(Pageable.class)))
                .thenReturn(appointmentPage);

        Page<Appointment> result = appointmentService.findAll("Dragana",  1, 10, "Dragana", true);

        verify(appointmentRepository).findByPatientFirstNameContainingIgnoreCaseOrPatientLastNameContainingIgnoreCase(eq("Dragana"), any(Pageable.class));

        assertThat(result).isEqualTo(appointmentPage);
    }

    @Nested
    class Exists {

        @Test
        void shouldThrowIllegalArgumentExceptionWhenIdIsNull() {

            IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> appointmentService.exists(null));

            assertEquals("Id must not be null", exception.getMessage());
        }

        @Test
        void shouldExist() {
            UUID appointmentId = UUID.fromString("bd9b7700-5000-4c72-9c60-2e4d955122ed");

            when(appointmentRepository.existsById(appointmentId)).thenReturn(true);

            boolean result = appointmentService.exists(appointmentId);

            verify(appointmentRepository).existsById(appointmentId);

            assertThat(result).isTrue();
        }

        @Test
        void shouldNotExist() {
            UUID appointmentId = UUID.fromString("dd6c1e8d-187e-4fea-824d-083333aec575");

            when(appointmentRepository.existsById(appointmentId)).thenReturn(false);

            boolean result = appointmentService.exists(appointmentId);

            verify(appointmentRepository).existsById(appointmentId);

            assertThat(result).isFalse();
        }
    }

    @Nested
    class Delete {
        @Test
        void shouldThrowIllegalArgumentExceptionWhenIdIsNull() {
            IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> appointmentService.delete(null));

            assertEquals("Id must not be null", exception.getMessage());
        }

        @Test
        void shouldDelete() {

            UUID id = UUID.fromString("f2ff581b-f15f-4a33-b1d9-135eba23694d");

            doNothing().when(appointmentRepository).deleteById(id);

            appointmentService.delete(id);

            verify(appointmentRepository, times(1)).deleteById(id);
        }
    }

    @Nested
    class GetAppointment {
        @Test
        void shouldGet() {
            UUID id = UUID.fromString("01f12b88-b65e-44f7-a9c5-6228c27e95ab");
            Patient patient = mock(Patient.class);

            Appointment createdAppointment = new Appointment(
                    id,
                    patient,
                    new Date(),
                    "12:45"
            );

            when(appointmentRepository.findById(id)).thenReturn(Optional.of(createdAppointment));

            Appointment result = appointmentService.get(id);

            verify(appointmentRepository).findById(id);

            assertThat(result.getId()).isEqualTo(createdAppointment.getId());
        }

        @Test
        void shouldThrowExceptionWhenAppointmentNotFound () {
            UUID id = UUID.fromString("7c32c06e-c296-42a8-8e36-2311566e6bc6");

            when(appointmentRepository.findById(id)).thenReturn(Optional.empty());

            assertThatThrownBy(() -> appointmentService.get(id))
                    .isInstanceOf(NoSuchElementException.class);

            verify(appointmentRepository).findById(id);
        }
    }

    @Test
    void shouldEdit() {

        UUID idFromAppointmentToBeEdited = UUID.fromString("a4e34f65-f387-446a-8bbe-6ada02071146");

        Patient patient = mock(Patient.class);

        Appointment appointmentToBeEdited = new Appointment(
                idFromAppointmentToBeEdited,
                patient,
                new Date(),
                "12:45"
        );

        Appointment editedAppointment = new Appointment();
        editedAppointment.setDate(new Date());
        editedAppointment.setId(idFromAppointmentToBeEdited);
        editedAppointment.setTime("12:45");
        editedAppointment.setPatient(patient);

        when(appointmentRepository.save(any())).thenReturn(editedAppointment);

        Appointment result = appointmentService.edit(appointmentToBeEdited);

        verify(appointmentRepository).save(appointmentToBeEdited);

        assertThat(result).isEqualTo(editedAppointment);
    }
}
package com.dentaloffice.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.dentaloffice.user.Permission.DOCTOR_DELETE;
import static com.dentaloffice.user.Permission.DOCTOR_CREATE;
import static com.dentaloffice.user.Permission.DOCTOR_UPDATE;
import static com.dentaloffice.user.Permission.DOCTOR_READ;

import static com.dentaloffice.user.Permission.NURSE_DELETE;
import static com.dentaloffice.user.Permission.NURSE_CREATE;
import static com.dentaloffice.user.Permission.NURSE_UPDATE;
import static com.dentaloffice.user.Permission.NURSE_READ;


@RequiredArgsConstructor
public enum Role {

    DOCTOR(
            Set.of(
                    DOCTOR_READ,
                    DOCTOR_UPDATE,
                    DOCTOR_CREATE,
                    DOCTOR_DELETE
            )
    ),
    NURSE(
            Set.of(
                    NURSE_READ,
                    NURSE_UPDATE,
                    NURSE_CREATE,
                    NURSE_DELETE
            )
    );

    @Getter
    private final Set<Permission> permissions;

    public List<SimpleGrantedAuthority> getAuthorities() {
        var authorities = getPermissions()
                .stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));

        return authorities;
    }
}

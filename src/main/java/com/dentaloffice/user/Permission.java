package com.dentaloffice.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Permission {

    DOCTOR_READ("doctor:read"),
    DOCTOR_UPDATE("doctor:update"),
    DOCTOR_CREATE("doctor:create"),
    DOCTOR_DELETE("doctor:delete"),
    NURSE_READ("nurse:read"),
    NURSE_UPDATE("nurse:update"),
    NURSE_CREATE("nurse:create"),
    NURSE_DELETE("nurse:delete");

    @Getter
    private final String permission;
}

package com.dentaloffice.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class MaterialRequestDTO {

    @NotNull(message = "Material name can not be null!")
    @Size(min=2, max= 25, message = "Material name must be grater than 1 character and not grater than 25 characters!")
    @Pattern(regexp = "^[a-zA-Z\\s\\-]*$", message = "Material name can not be a number!")
    private String materialName;

    @NotNull(message = "Quantity can not be null!")
    @Min(value = 1, message = "Value must be greater than zero")
    @Max(value = 99999, message = "Quantity must be at most 99999.")
    private Integer quantity;
}

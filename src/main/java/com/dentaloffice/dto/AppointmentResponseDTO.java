package com.dentaloffice.dto;

import com.dentaloffice.models.Patient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppointmentResponseDTO {
    private UUID id;

    private String date;
    private String time;
    private Patient patient;
}

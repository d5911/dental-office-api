package com.dentaloffice.services;

import com.dentaloffice.models.DentalService;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

public interface DentalServiceService {

    DentalService save(DentalService service);

    Page<DentalService> findAll(String filter, Integer pageNo, Integer pageSize, String sortKey, boolean sortAsc);

    List<DentalService> getServices(UUID recordId);

    void delete(UUID id);

    DentalService edit(DentalService editedDentalService);

    DentalService get(UUID id);

    boolean exists(UUID id);
}

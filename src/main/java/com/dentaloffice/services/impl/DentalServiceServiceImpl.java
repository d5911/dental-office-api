package com.dentaloffice.services.impl;

import com.dentaloffice.repositories.DentalServiceRepository;
import com.dentaloffice.models.DentalService;
import com.dentaloffice.services.DentalServiceService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@Service
@AllArgsConstructor
@Log
public class DentalServiceServiceImpl implements DentalServiceService {

    private final DentalServiceRepository dentalServiceRepository;

    @Override
    public DentalService save(DentalService service) {

        log.info("Saving dental service data to DB " + service);

        if(service == null) {
            throw new IllegalArgumentException("Service must not be null");
        }

        return dentalServiceRepository.save(service);
    }

    @Override
    public Page<DentalService> findAll(String filter, Integer pageNo, Integer pageSize, String sort, boolean sortAsc) {

        log.info("Retrieving all dental service data from DB");

        Sort sortDirection = sortAsc ? Sort.by(sort).ascending() : Sort.by(sort).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sortDirection);

        return dentalServiceRepository.findByServiceNameContainingIgnoreCase(filter, pageable);
    }

    @Override
    public List<DentalService> getServices(UUID recordId) {

        log.info("Getting dental service from DB with id " + recordId);

        return dentalServiceRepository.findByEnrolledServicesInRecordId(recordId);
    }

    public boolean exists(UUID id){

        log.info("Checking if dental service exists in DB with id " + id);

        return dentalServiceRepository.existsById(id);
    }

    @Override
    public void delete(UUID id) {

        log.info("Deleting dental service from DB with id " + id);

        if(id == null) {
            throw new IllegalArgumentException("Id must not be null");
        }

        dentalServiceRepository.deleteById(id);
    }

    @Override
    public DentalService edit(DentalService editedDentalService) {

        log.info("Editing dental service in DB " + editedDentalService);

        return dentalServiceRepository.save(editedDentalService);
    }

    @Override
    public DentalService get(UUID id) {

        log.info("Getting dental service from DB with id " + id);

        return  dentalServiceRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Element not found with id: " + id));
    }
}

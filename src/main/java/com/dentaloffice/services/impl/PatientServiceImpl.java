package com.dentaloffice.services.impl;
//import com.dentaloffice.exceptions.PatientNotFoundException;
import com.dentaloffice.models.Material;
import com.dentaloffice.models.Patient;
import com.dentaloffice.repositories.PatientRepository;
import com.dentaloffice.services.PatientService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
@Log
public class PatientServiceImpl implements PatientService {

    private final PatientRepository patientRepository;

    @Override
    public Patient save(Patient patient) {

        log.info("Saving patient data to DB " + patient);

        return patientRepository.save(patient);
    }

    @Override
    public Page<Patient> findAll(String filter, Integer pageNo, Integer pageSize, String sort, Boolean sortAsc) {

        log.info("Retrieving all patient data from DB");

        Sort sortDirection = sortAsc ? Sort.by(sort).ascending() : Sort.by(sort).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sortDirection);

        return patientRepository.findByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCase(filter, filter, pageable);
    }

    @Override
    public boolean exists(UUID id){

        log.info("Checking if patient exist in DB with id " + id);

        return patientRepository.existsById(id);
    }

    @Override
    public void delete(UUID id) {

        log.info("Deleting patient from DB with id " + id);

        patientRepository.deleteById(id);
    }

    @Override
    public Patient edit(Patient patientToBeEdited) {

        log.info("Editing patient data in DB " + patientToBeEdited);

        if (patientToBeEdited.getId() != null) {
            return patientRepository.save(patientToBeEdited);
        }
        throw new IllegalArgumentException("Patient id is null!");
    }

    @Override
    public Patient get(UUID id) {

        log.info("Getting patient from DB with id " + id);

        Patient patient = patientRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Element not found with id: " + id));

        return new Patient(patient.getId(), patient.getFirstName(), patient.getLastName(), patient.getBirthDate(), patient.getPhoneNumber(), null);
    }
}

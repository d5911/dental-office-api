package com.dentaloffice.services.impl;

import com.dentaloffice.models.Material;
import com.dentaloffice.models.Record;
import com.dentaloffice.repositories.MaterialRepository;
import com.dentaloffice.repositories.RecordRepository;
import com.dentaloffice.services.MaterialService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
@Log
public class MaterialServiceImpl implements MaterialService {

    private final MaterialRepository materialRepository;
    private final RecordRepository recordRepository;


    @Override
    public Material save(Material material) {

        log.info("Saving material to DB " + material);

        return materialRepository.save(material);
    }

    @Override
    public Page<Material> findAll(String filter, Integer pageNo, Integer pageSize, String sort, boolean sortAsc) {

        log.info("Retrieving all material data from DB");

        Sort sortDirection = sortAsc ? Sort.by(sort).ascending() : Sort.by(sort).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sortDirection);

        return materialRepository.findByMaterialNameContainingIgnoreCase(filter, pageable);
    }

    public boolean exists(UUID id) {
        return materialRepository.existsById(id);
    }

    @Override
    @Transactional
    public void delete(UUID id) {

        log.info("Deleting material from DB with id " + id);

        materialRepository.deleteById(id);
    }

    @Override
    public Material edit(Material editedMaterial) {

        log.info("Editing material data in DB " + editedMaterial);

        if(editedMaterial.getId() != null) {
            return materialRepository.save(editedMaterial);
        }
        throw new IllegalArgumentException("Material id is null");

    }

    @Override
    public Material get(UUID id) {

        log.info("Getting material from DB with id " + id);

        Material material = materialRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Element not found with id: " + id));;

        return new Material(material.getId(), null, material.getMaterialName(), material.getQuantity());

    }

    @Override
    public List<Material> getMaterials(UUID recordId) {

        log.info("Getting used materials from DB with record id " + recordId);

        return materialRepository.findByEnrolledMaterialsInRecordId(recordId);
    }

    @Override
    @Transactional
    public void addRecord(UUID materialId, UUID recordId) throws Exception {

        log.info("Adding used material with id " + materialId + " to the record with id " + recordId);

        Material material = materialRepository.getById(materialId);
        List<Record> enrolledMaterials = material.getEnrolledMaterialsInRecord();
        Record record = recordRepository.getById(recordId);

        if (recordRepository.findByIdAndMaterialsId(recordId, materialId).isPresent()) {
            throw new Exception();
        }
        enrolledMaterials.add(record);
    }
}

package com.dentaloffice.services.impl;

import com.dentaloffice.models.DentalService;
import com.dentaloffice.models.Material;
import com.dentaloffice.models.Patient;
import com.dentaloffice.models.Record;
import com.dentaloffice.repositories.*;

import com.dentaloffice.services.RecordService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@AllArgsConstructor
@Log
public class RecordServiceImpl implements RecordService {

    private final RecordRepository recordRepository;
    private final PatientRepository patientRepository;
    private final MaterialRepository materialRepository;
    private final DentalServiceRepository serviceRepository;

    @Override
    @Transactional
    public Record save(UUID patientId, List<UUID> materialIds, List<UUID> serviceIds, String description, List<Integer> numberOfUsedMaterials) {

        log.info("Saving record data to DB " + patientId + " " + materialIds + " " + serviceIds + " " + description);

        Date currentDate = new Date();

        Patient patientById = patientRepository.getById(patientId);
        Patient simplePatientData = new Patient(patientById.getId(), patientById.getFirstName(), patientById.getLastName(), patientById.getBirthDate(), patientById.getPhoneNumber(), null);

        Record record = new Record();
        record.setDate(currentDate);
        record.setPatient(simplePatientData);
        record.setDescription(description);

        for(int i=0; i<materialIds.size(); i++){
            Material existingMaterial = materialRepository.getById(materialIds.get(i));
            Integer quantity = existingMaterial.getQuantity() - numberOfUsedMaterials.get(i);
            existingMaterial.setQuantity(quantity);
            existingMaterial.getEnrolledMaterialsInRecord().add(record);
            record.getMaterials().add(existingMaterial);
        }

        for (UUID serviceId : serviceIds) {
            DentalService existingService = serviceRepository.getById(serviceId);
            existingService.getEnrolledServicesInRecord().add(record);
            record.getServices().add(existingService);
        }

        Record savedRecord = recordRepository.save(record);

        Patient savedPatient = savedRecord.getPatient();
        List<Material> savedMaterials = savedRecord.getMaterials();
        List<DentalService> savedServices = savedRecord.getServices();

        return new Record(savedRecord.getId(), currentDate, savedPatient, description, savedMaterials, savedServices);
    }

    @Override
    public Page<Record> findAll(String filter, Integer pageNo, Integer pageSize, String sort, Boolean sortAsc) {

        log.info("Retrieving all record data from DB");

        Sort sortDirection = sortAsc ? Sort.by(sort).ascending() : Sort.by(sort).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sortDirection);

        return recordRepository.findByPatientFirstNameContainingIgnoreCaseOrPatientLastNameContainingIgnoreCase(filter, filter, pageable);
    }

    public boolean exists(UUID id){

        log.info("Checking if record exists in DB with id " + id);

        return recordRepository.existsById(id);
    }

}

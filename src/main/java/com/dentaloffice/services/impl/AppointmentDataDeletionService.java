package com.dentaloffice.services.impl;

import com.dentaloffice.repositories.AppointmentRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@AllArgsConstructor
public class AppointmentDataDeletionService {

    @Autowired
    private final AppointmentRepository appointmentRepository;


    @Transactional
    public void deleteOldData(Date date){
        this.appointmentRepository.deleteByDateBefore(date);
    }
}

package com.dentaloffice.services.impl;

import com.dentaloffice.models.Appointment;
import com.dentaloffice.repositories.AppointmentRepository;
import com.dentaloffice.services.AppointmentService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
@Log
public class AppointmentServiceImpl implements AppointmentService {

    private final AppointmentRepository appointmentRepository;

    @Override
    public Appointment save(Appointment appointment) {
        log.info("Saving appointment data to DB" + appointment);

//        return appointmentRepository.save(null);

//        Appointment save = appointmentRepository.save(appointment);
//        return new Appointment();

//        return appointment;

        if(appointment == null) {
            throw new IllegalArgumentException("Appointment must not be null");
        }
        return  appointmentRepository.save(appointment);
    }

    @Override
    public Page<Appointment> findAll(String filter, Integer pageNo, Integer pageSize, String sort, Boolean sortAsc) {

        log.info("Retrieving all data from appointment entity");

        Sort sortDirection = sortAsc ? Sort.by(sort).ascending() : Sort.by(sort).descending();
        Pageable pageable = PageRequest.of(pageNo, pageSize, sortDirection);

        return appointmentRepository.findByPatientFirstNameContainingIgnoreCaseOrPatientLastNameContainingIgnoreCase(filter, pageable);
    }

    public boolean exists(UUID id){
        log.info("Checking if appointment exist in DB with id " + id);

        if(id == null) {
            throw new IllegalArgumentException("Id must not be null");
        }

        return appointmentRepository.existsById(id);
    }

    @Override
    public void delete(UUID id) {

        log.info("Deleting appointment data fron DB with id " + id);

        if(id == null) {
            throw new IllegalArgumentException("Id must not be null");
        }

        appointmentRepository.deleteById(id);
    }

    @Override
    public Appointment get(UUID id) {

        log.info("Getting appointment from DB with id " + id);

        return  appointmentRepository.findById(id)
                .orElseThrow(() -> new NoSuchElementException("Element not found with id: " + id));
    }

    @Override
    public Appointment edit(Appointment editedAppointment) {

        log.info("Editing appointment data in a DB " + editedAppointment);

        return appointmentRepository.save(editedAppointment);
    }
}

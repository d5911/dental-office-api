package com.dentaloffice.services;

import com.dentaloffice.models.Record;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

public interface RecordService {

    Record save(UUID patientId, List<UUID> materialIds, List<UUID> serviceIds, String description, List<Integer> numberOfUsedMaterials);

    Page<Record> findAll(String searchTerm, Integer pageNo, Integer pageSize, String sort, Boolean sortAsc);

    boolean exists(UUID id);

}

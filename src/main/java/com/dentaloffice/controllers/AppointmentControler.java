package com.dentaloffice.controllers;

import com.dentaloffice.dto.AppointmentRequestDTO;
import com.dentaloffice.dto.AppointmentResponseDTO;
import com.dentaloffice.dto.PageResponse;
import com.dentaloffice.models.Appointment;
import com.dentaloffice.models.Patient;
import com.dentaloffice.services.AppointmentService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(AppointmentControler.BASE_URL)
@AllArgsConstructor
@Log
@PreAuthorize("hasRole('NURSE')")
public class AppointmentControler {

    public static final String BASE_URL = "/appointment";
    private final AppointmentService appointmentService;

    @PostMapping
    @PreAuthorize("hasAuthority('nurse:create')")
    public Appointment save(@Valid @RequestBody AppointmentRequestDTO appointment) {

        log.info("Received data from client for saving to appointment: " + appointment);

        Appointment appointmentToBeSaved = new Appointment();
        Patient patient = new Patient();
        patient.setId(appointment.getPatientId());
        appointmentToBeSaved.setPatient(patient);
        appointmentToBeSaved.setDate(appointment.getDate());
        appointmentToBeSaved.setTime(appointment.getTime());
        return appointmentService.save(appointmentToBeSaved);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('nurse:read')")
    public PageResponse<AppointmentResponseDTO> findAll(@RequestParam(name="searchTerm", required = false) String searchTerm,
                                                        @RequestParam(defaultValue = "0") Integer pageNo,
                                                        @RequestParam(defaultValue = "10") Integer pageSize,
                                                        @RequestParam(name = "sort", defaultValue = "date", required = false) String sort,
                                                        @RequestParam(name = "sortAsc", defaultValue = "false") Boolean sortAsc) {

        log.info("Retrieving all data from /appointment endpoint");

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        Page<Appointment> pagedAppointments = appointmentService.findAll(searchTerm, pageNo, pageSize, sort, sortAsc);
        List<Appointment> appointments = pagedAppointments.getContent();

        List<AppointmentResponseDTO> convertedAppointments = appointments.stream()
                .map(appointment -> {
                    Patient patient = appointment.getPatient();
                    Patient patientCopy = new Patient(patient.getId(), patient.getFirstName(), patient.getLastName(), patient.getBirthDate(), patient.getPhoneNumber(), null);
                    return new AppointmentResponseDTO(appointment.getId(), formatter.format(appointment.getDate()), appointment.getTime(), patientCopy);
                })
                .collect(Collectors.toList());

        PageResponse<AppointmentResponseDTO> response = new PageResponse<>();
        response.setContent(convertedAppointments);
        response.setTotalPages(pagedAppointments.getTotalPages());

        return response;
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:delete')")
    public void delete(@PathVariable UUID id) {

        log.info("Deleting appointment with id " + id);

        if (!appointmentService.exists(id)) {
            throwNotFoundException(id);
        }
        appointmentService.delete(id);

    }

    @GetMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:read')")
    public AppointmentResponseDTO get(@PathVariable UUID id) {

        log.info("Retriveving appointment with id " + id);

        Appointment appointmentDb = appointmentService.get(id);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Patient patientCopy = new Patient(appointmentDb.getPatient().getId(), appointmentDb.getPatient().getFirstName(), appointmentDb.getPatient().getLastName(), appointmentDb.getPatient().getBirthDate(), appointmentDb.getPatient().getPhoneNumber(), null);

        return new AppointmentResponseDTO(appointmentDb.getId(), formatter.format(appointmentDb.getDate()), appointmentDb.getTime(), patientCopy);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:update')")
    public AppointmentResponseDTO edit(@PathVariable UUID id, @Valid @RequestBody AppointmentRequestDTO appointment) {

        log.info("Editing apointment data with id " + id);

        Appointment appointmentToBeSaved = new Appointment();

        appointmentToBeSaved.setId(id);

        Patient patient = new Patient();
        patient.setId(appointment.getPatientId());

        appointmentToBeSaved.setPatient(patient);

        appointmentToBeSaved.setDate(appointment.getDate());
        appointmentToBeSaved.setTime(appointment.getTime());

        if (!appointmentService.exists(id)) {
            throwNotFoundException(id);
        }

        Appointment editedAppointment = appointmentService.edit(appointmentToBeSaved);

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        Patient patientDB = editedAppointment.getPatient();
        Patient patientDBCopy = new Patient(patientDB.getId(), patientDB.getFirstName(), patientDB.getLastName(), patientDB.getBirthDate(), patientDB.getPhoneNumber(), null);

        return new AppointmentResponseDTO(editedAppointment.getId(), formatter.format(editedAppointment.getDate()), editedAppointment.getTime(), patientDBCopy);
    }

    private void throwNotFoundException(UUID id) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data with id " + id + " exist");
    }
}
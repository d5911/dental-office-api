package com.dentaloffice.controllers;
import com.dentaloffice.dto.PageResponse;
import com.dentaloffice.dto.PatientRequestDTO;
import com.dentaloffice.dto.PatientResponseDTO;
import com.dentaloffice.models.Patient;
import com.dentaloffice.services.PatientService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(PatientController.BASE_URL)
@AllArgsConstructor
@Log
@PreAuthorize("hasAnyRole('NURSE') or ('DOCTOR')")
public class PatientController {

    public static final String BASE_URL = "/patient";
    private final PatientService patientService;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('nurse:read','doctor:read')")
    public PageResponse<PatientResponseDTO> findAll(@RequestParam(name = "searchTerm", required = false) String searchTerm,
                                                    @RequestParam(defaultValue = "0") Integer pageNo,
                                                    @RequestParam(defaultValue = "10") Integer pageSize,
                                                    @RequestParam(name = "sort", defaultValue = "firstName", required = false) String sort,
                                                    @RequestParam(name= "sortAsc", defaultValue = "false") Boolean sortAsc) {

        log.info("Retrieving all data on /patient endpoint");

        Page<Patient> pagedPatients = patientService.findAll(searchTerm, pageNo, pageSize, sort, sortAsc);

        List<Patient> patients = pagedPatients.getContent();
        PageResponse<PatientResponseDTO> response = new PageResponse<>();

        List<PatientResponseDTO> convertedPatients = patients.stream()
                .map(patient -> new PatientResponseDTO(patient.getId(), patient.getFirstName(), patient.getLastName(), patient.getBirthDate(), patient.getPhoneNumber()))
                .collect(Collectors.toList());

        response.setContent(convertedPatients);
        response.setTotalPages(pagedPatients.getTotalPages());

        return response;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('nurse:create')")
    public Patient save(@Valid @RequestBody PatientRequestDTO patient) {

        log.info("Receiving data from client for saving to patient " + patient);

        Patient patientToBeSaved = new Patient();
        patientToBeSaved.setFirstName(patient.getFirstName());
        patientToBeSaved.setLastName(patient.getLastName());
        patientToBeSaved.setBirthDate(patient.getBirthDate());
        patientToBeSaved.setPhoneNumber(patient.getPhoneNumber());

        return patientService.save(patientToBeSaved);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:delete')")
    public void delete(@PathVariable UUID id) {

        log.info("Deleting patient with id " + id);

        if (!patientService.exists(id)) {
            throwNotFoundException(id);
        }

        try {
            patientService.delete(id);
        } catch (DataIntegrityViolationException exception) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:read')")
    public PatientResponseDTO get(@PathVariable UUID id) {
        log.info("Getting patient with id " + id);

        Patient patientDB = patientService.get(id);

        return new PatientResponseDTO(patientDB.getId(), patientDB.getFirstName(), patientDB.getLastName(), patientDB.getBirthDate(), patientDB.getPhoneNumber());
    }

    @PutMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:update')")
    public Patient edit(@PathVariable UUID id, @Valid @RequestBody PatientRequestDTO patient) {

        log.info("Editing patient with id " + id);

        Patient patientToBeSaved = new Patient();
        patientToBeSaved.setId(id);
        patientToBeSaved.setFirstName(patient.getFirstName());
        patientToBeSaved.setLastName(patient.getLastName());
        patientToBeSaved.setBirthDate(patient.getBirthDate());
        patientToBeSaved.setPhoneNumber(patient.getPhoneNumber());

        if (!patientService.exists(id)) {
            throwNotFoundException(id);
        }

        return patientService.edit(patientToBeSaved);
    }

    private void throwNotFoundException(UUID id) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data with id " + id + " exist");
    }
}

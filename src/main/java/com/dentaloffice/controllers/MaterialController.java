package com.dentaloffice.controllers;

import com.dentaloffice.dto.MaterialRequestDTO;
import com.dentaloffice.dto.MaterialResponseDTO;
import com.dentaloffice.dto.PageResponse;
import com.dentaloffice.models.Material;
import com.dentaloffice.services.MaterialService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(MaterialController.BASE_URL)
@AllArgsConstructor
@Log
@PreAuthorize("hasRole('NURSE')")
public class MaterialController {

    public static final String BASE_URL = "/material";
    private final MaterialService materialService;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('nurse:read','doctor:read')")
    public PageResponse<MaterialResponseDTO> findAll(@RequestParam(name = "searchTerm", required = false) String searchTerm,
                                                     @RequestParam(defaultValue = "0") Integer pageNo,
                                                     @RequestParam(defaultValue = "10") Integer pageSize,
                                                     @RequestParam(name = "sort", defaultValue = "materialName", required = false) String sort,
                                                     @RequestParam(name = "sortAsc", defaultValue = "false") Boolean sortAsc) {

        log.info("Retrieving all data on /material endpoint");

        Page<Material> pagedMaterials = materialService.findAll(searchTerm, pageNo, pageSize, sort, sortAsc);

        List<Material> materials = pagedMaterials.getContent();

        List<MaterialResponseDTO> convertedMaterials = materials.stream()
                .map(item -> new MaterialResponseDTO(item.getId(), item.getMaterialName(), item.getQuantity()))
                .collect(Collectors.toList());

        PageResponse<MaterialResponseDTO> response = new PageResponse<>();
        response.setContent(convertedMaterials);
        response.setTotalPages(pagedMaterials.getTotalPages());

        return response;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('nurse:create')")
    public Material save(@Valid @RequestBody MaterialRequestDTO material) {

        log.info("Receiving data from client for saving to material " + material);

        Material materialToBeSaved = new Material();
        materialToBeSaved.setMaterialName(material.getMaterialName());
        materialToBeSaved.setQuantity(material.getQuantity());

        return materialService.save(materialToBeSaved);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:delete')")
    public void delete(@PathVariable UUID id) {

        log.info("Deleting material data with id " + id);

        if (!materialService.exists(id)) {
            throwNotFoundException(id);
        }

        try {
            materialService.delete(id);
        } catch (DataIntegrityViolationException exception) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }

    }

    @GetMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:read')")
    public MaterialResponseDTO get(@PathVariable UUID id) {

        log.info("Retrieving material data with id " + id);

        Material materialDB = materialService.get(id);

        return new MaterialResponseDTO(materialDB.getId(), materialDB.getMaterialName(), materialDB.getQuantity());
    }

    @PutMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:update')")
    public Material edit(@PathVariable UUID id, @Valid @RequestBody MaterialRequestDTO material) {

        log.info("Editing material data with id " + id);

        Material materialToBeSaved = new Material();
        materialToBeSaved.setId(id);
        materialToBeSaved.setMaterialName(material.getMaterialName());
        materialToBeSaved.setQuantity(material.getQuantity());

        if (!materialService.exists(id)) {
            throwNotFoundException(id);
        }

        return materialService.edit(materialToBeSaved);
    }

    private void throwNotFoundException(UUID id) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data with id " + id + " exist");
    }

    @PutMapping("/{materialId}/records/{recordId}")
    @PreAuthorize("hasAuthority('nurse:update')")
    ResponseEntity<Material> addRecord(@PathVariable UUID materialId, @PathVariable UUID recordId) {

        log.info("Adding used material with id " + materialId + " for adding those data to record on id " + recordId);

        try {
            materialService.addRecord(materialId, recordId);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }
}

package com.dentaloffice.controllers;

import com.dentaloffice.dto.DentalServiceRequestDTO;
import com.dentaloffice.dto.DentalServiceResponseDTO;
import com.dentaloffice.dto.PageResponse;
import com.dentaloffice.models.DentalService;
import com.dentaloffice.services.DentalServiceService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(DentalServicesController.BASE_URL)
@AllArgsConstructor
@Log
@PreAuthorize("hasRole('NURSE')")
public class DentalServicesController {

    public static final String BASE_URL = "/dentalService";
    private final DentalServiceService dentalServiceService;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('nurse:read','doctor:read')")
    public PageResponse<DentalServiceResponseDTO> findAll(@RequestParam(name = "searchTerm", required = false) String searchTerm,
                                               @RequestParam(defaultValue = "0") Integer pageNo,
                                               @RequestParam(defaultValue = "10") Integer pageSize,
                                               @RequestParam(name = "sort", defaultValue = "serviceName", required = false) String sort,
                                               @RequestParam(name = "sortAsc", defaultValue = "false") Boolean sortAsc) {

        log.info("Retrieving all data on /dentalService endpoint");

        Page<DentalService> pagedDentalServices = dentalServiceService.findAll(searchTerm, pageNo, pageSize, sort, sortAsc);
        List<DentalService> dentalServices = pagedDentalServices.getContent();

        List<DentalServiceResponseDTO> convertedDentalServices = dentalServices.stream()
                .map(dentalService -> new DentalServiceResponseDTO(dentalService.getId(), dentalService.getServiceName()))
                .collect(Collectors.toList());

        PageResponse<DentalServiceResponseDTO> response = new PageResponse<>();
        response.setContent(convertedDentalServices);
        response.setTotalPages(pagedDentalServices.getTotalPages());

        return response;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('nurse:create')")
    public DentalService save(@Valid @RequestBody DentalServiceRequestDTO dentalService) {

        log.info("Receiving data from client for saving to dental service " + dentalService);

        DentalService dentalServiceToBeSaved = new DentalService();
        dentalServiceToBeSaved.setServiceName(dentalService.getServiceName());

        return dentalServiceService.save(dentalServiceToBeSaved);
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:read')")
    public DentalServiceResponseDTO get(@PathVariable UUID id) {

        log.info("Retrieving dental service data with id " + id);

        DentalService dentalService = dentalServiceService.get(id);

        return new DentalServiceResponseDTO(dentalService.getId(), dentalService.getServiceName());
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:delete')")
    public void delete(@PathVariable UUID id) {

        log.info("Deleting dental service data with id " + id);

        if (!dentalServiceService.exists(id)) {
            throwNotFoundException(id);
        }

        try {
            dentalServiceService.delete(id);
        } catch (DataIntegrityViolationException exception) {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    @PutMapping("{id}")
    @PreAuthorize("hasAuthority('nurse:update')")
    public DentalService edit(@PathVariable UUID id, @Valid @RequestBody DentalServiceRequestDTO dentalServiceRequestDTO) {

        log.info("Editing dental service data with id " + id);

        DentalService dentalServiceToBeSaved = new DentalService();
        dentalServiceToBeSaved.setId(id);
        dentalServiceToBeSaved.setServiceName(dentalServiceRequestDTO.getServiceName());

        if (!dentalServiceService.exists(id)) {
            throwNotFoundException(id);
        }

        return dentalServiceService.edit(dentalServiceToBeSaved);
    }

    private void throwNotFoundException(UUID id) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No data with id " + id + " exist");
    }
}

package com.dentaloffice.controllers;

import com.dentaloffice.dto.*;
import com.dentaloffice.models.DentalService;
import com.dentaloffice.models.Material;
import com.dentaloffice.models.Patient;
import com.dentaloffice.models.Record;

import com.dentaloffice.services.DentalServiceService;
import com.dentaloffice.services.MaterialService;
import com.dentaloffice.services.RecordService;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(RecordController.BASE_URL)
@AllArgsConstructor
@Log
@PreAuthorize("hasRole('DOCTOR')")
public class RecordController {

    public static final String BASE_URL = "/record";
    private final RecordService recordService;
    private final MaterialService materialService;
    private final DentalServiceService dentalServiceService;

    @PostMapping
    @PreAuthorize("hasAuthority('doctor:create')")
    public RecordResponseDTO save(@Valid @RequestBody RecordRequestDTO record) {

        log.info("Received data from client for saving to record: " + record);

        UUID patientId = UUID.fromString(record.getPatientId());
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Record dataForRecord = recordService.save(patientId, record.getMaterialIds(), record.getServiceIds(), record.getDescription(), record.getNumberOfUsedMaterials());

        Patient savedPatient = dataForRecord.getPatient();
        Patient savedPatientCopy = new Patient(savedPatient.getId(), savedPatient.getFirstName(), savedPatient.getLastName(), savedPatient.getBirthDate(), savedPatient.getPhoneNumber(), null);

        return new RecordResponseDTO(dataForRecord.getId(), formatter.format(dataForRecord.getDate()), savedPatientCopy, record.getDescription());
    }

    @GetMapping
    @PreAuthorize("hasAuthority('doctor:read')")
    public PageResponse<RecordResponseDTO> findAll(@RequestParam(name="searchTerm", required = false) String searchTerm,
                                        @RequestParam(defaultValue = "0") Integer pageNo,
                                        @RequestParam(defaultValue = "10") Integer pageSize,
                                        @RequestParam(name = "sort", defaultValue = "firstName", required = false) String sort,
                                        @RequestParam(name = "sortAsc", defaultValue = "false") Boolean sortAsc) {

        log.info("Retrieving all data from /record endpoint");

        Page<Record> pagedResult = recordService.findAll(searchTerm, pageNo, pageSize, sort, sortAsc);

        List<Record> records = pagedResult.getContent();

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        List<RecordResponseDTO> convertedRecords = records.stream()
                .map(item -> {
                    Patient patient = item.getPatient();
                    Patient patientCopy = new Patient(patient.getId(), patient.getFirstName(), patient.getLastName(), patient.getBirthDate(), patient.getPhoneNumber(), null);

                    return new RecordResponseDTO(item.getId(), formatter.format(item.getDate()), patientCopy, item.getDescription());
                })
                .collect(Collectors.toList());

        PageResponse<RecordResponseDTO> response = new PageResponse<>();
        response.setContent(convertedRecords);
        response.setTotalPages(pagedResult.getTotalPages());

        return response;
    }

    @GetMapping("{recordId}/materials")
    @PreAuthorize("hasAuthority('doctor:read')")
    public List<MaterialResponseDTO> getMaterialsByRecordId(@PathVariable UUID recordId) {

        log.info("Requested materials for record " + recordId);

        List<Material> materialsInRecord = materialService.getMaterials(recordId);

        return materialsInRecord.stream().map(item -> new MaterialResponseDTO(item.getId(), item.getMaterialName(), item.getQuantity())).collect(Collectors.toList());
    }

    @GetMapping("{recordId}/services")
    @PreAuthorize("hasAuthority('doctor:read')")
    public List<DentalServiceResponseDTO> getServicesByRecordId(@PathVariable UUID recordId) {

        log.info("Requested services for record " + recordId);

        List<DentalService> servicesInRecord = dentalServiceService.getServices(recordId);

        return servicesInRecord.stream().map(item -> new DentalServiceResponseDTO(item.getId(), item.getServiceName())).collect(Collectors.toList());
    }
}

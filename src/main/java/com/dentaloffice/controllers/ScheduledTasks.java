package com.dentaloffice.controllers;

import com.dentaloffice.services.impl.AppointmentDataDeletionService;
import lombok.AllArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;
import java.util.Date;

@CommonsLog
@RestController
@AllArgsConstructor
public class ScheduledTasks {

    @Autowired
    private final AppointmentDataDeletionService appointmentDataDeletionService;


    @Scheduled(cron = "0 0 0 * * *")
    public void deleteOldDataJob(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        Date deletionDate = calendar.getTime();

        appointmentDataDeletionService.deleteOldData(deletionDate);
    }
}

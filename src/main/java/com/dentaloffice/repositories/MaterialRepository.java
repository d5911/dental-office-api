package com.dentaloffice.repositories;

import com.dentaloffice.models.Material;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MaterialRepository extends JpaRepository<Material, UUID> {

    @EntityGraph(attributePaths = "enrolledMaterialsInRecord")
    Page<Material> findByMaterialNameContainingIgnoreCase(String materialName, Pageable pageable);

    List<Material> findByEnrolledMaterialsInRecordId(UUID recordId);

}

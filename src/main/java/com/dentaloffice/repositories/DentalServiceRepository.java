package com.dentaloffice.repositories;

import com.dentaloffice.models.DentalService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface DentalServiceRepository extends JpaRepository<DentalService, UUID> {

    @EntityGraph(attributePaths = "enrolledServicesInRecord")
    Page<DentalService> findByServiceNameContainingIgnoreCase(String serviceName, Pageable pageable);

    List<DentalService> findByEnrolledServicesInRecordId(UUID recordId);
}
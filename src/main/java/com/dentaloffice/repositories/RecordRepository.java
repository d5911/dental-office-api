package com.dentaloffice.repositories;

import com.dentaloffice.models.Record;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface RecordRepository extends JpaRepository<Record, UUID> {

    @EntityGraph(attributePaths = "patient")
    Page<Record> findByPatientFirstNameContainingIgnoreCaseOrPatientLastNameContainingIgnoreCase(String filter, String filter2, Pageable pageable);

    Optional<Record> findByIdAndMaterialsId(UUID recordId, UUID materialId);

}

-- Add a new integer column
ALTER TABLE material ADD COLUMN new_quantity integer;

-- Update the new column with the data from the old column
UPDATE material SET new_quantity = CAST(quantity AS integer);

-- Drop the old column
ALTER TABLE material DROP COLUMN quantity;

-- Rename the new column to the original name if needed
ALTER TABLE material RENAME COLUMN new_quantity TO quantity;

